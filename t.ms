.so resume.tmac
.
.title First and Last name
.subtitle Your desiered position
.
.sp -0.2i
.mk
.po 5.6i
.section Personal Information

.SM
.SM
.fi
\fBGitHub:\fR
.pdfhref W -D https://github.com/github github
.br
\fBGitLab:\fR
.pdfhref W -D https://gitlab.com/github gitlab
.br
\fBLinkedIn:\fR
.pdfhref W -D https://linkedin.com/in/linkedin-profile linkedin-profile
.br
\fBBlog:\fR
.pdfhref W -D https://example.com example.com
.br
\fBLocation:\fR
Location
.br
\fBPhone:\fR
+98 912 345 6780
.br
\fBEmail:\fR
.pdfhref W -D mailto:example@email.com example@email.com
.

.
.rt
.po 0.5i
.nr LL -2.4i
.nr FM 0.5i
.section Skills
.nf
.subsection Programming
.skill "Lua,"
.skill "C,"
.skill "Python,"
.skill "Bash,"
.skill "Go"
.subsection Tools
.skill "Linux,"
.skill "Docker,"
.skill "Kubernetes,"
.skill "Jenkins,"
.skill "Helm,"
.skill "QEMU/KVM,"
.skill "Makefile,"
.skill "vim,"
.skill "tmux,"
.skill "git,"
.skill "entr,"
.skill "crontab"

.section Work Experiences
.
.nf
.item "Self-hosting services on VPS" "Database management" "2019 - Now" "Remote"
Setup and support many services like LAMP/LEMP, SMTP/IMAP/POP and git on bare metal/virtual servers.
.sep -2.4
.item "Linux System Administrator and Automation" "RPS Cloud" "1401" "Remote"
VPS and Cloud service provider
.sep -2.4
.item "DevOps mentoring and Linux bootcamp" "Linux, Docker, Kubernetes, Helm, CI/CD" "2021 - Now" "BigBlueButton"

.sep -2.4
.list LPIC-1
.list Docker Deep Dive
.list Kubernetes in Action
.list Pipeline as Code
