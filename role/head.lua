M = {}

M.info = {
	name = "First name Last name",
	location = "Location",
	email = "example@email.com",
	github = "github",
	gitlab = "gitlab",
	phone = "+98 912 345 6780",
	blog = "example.com",
	linkedin = "linkedin-profile",
	pic = "tmp.png",
}

M.languages = {

	-- {
	-- 	name = "Persian",
	-- 	desc = "Native",
	-- },

	{
		name = "English",
		desc = "Professional",
	},

	{
		name = "German",
		desc = "Basics",
	},

}

M.education = {

	{
		name = "Computer Science",
		major = "Bachelors",
		date = "2019 -- 2024",
	},

}

M.interests = {
	"Bikes",
	"Fishing",
	"Sudoku",
--	"طراحی/تعمیر مدار",
	"Writing",
	"Voleyball",
}

M.certifications = {

	{
		name = "LPIC 1",
		date = "2019",
		des = [[]],
	},

	{
		name = "LPIC 2",
		date = "2020",
		des = [[]],
	},

}

return M
