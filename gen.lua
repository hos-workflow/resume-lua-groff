#!/usr/bin/env lua5.4

local info_path = "head"
local role_path = "pos"

local base_status, base = pcall(require, string.format("role.%s", info_path))
local role_status, template = pcall(require, string.format("role.%s", role_path))

if not role_status or not base_status then
	os.exit(1)
end

-- user info {{{
local info = {
	user = {
		name = base.info.name,
		location = base.info.location,
		email = base.info.email,
		github = base.info.github,
		gitlab = base.info.gitlab,
		phone = base.info.phone,
		blog = base.info.blog,
		linkedin = base.info.linkedin,
		pic = pic_path or base.info.pic,
		pos = template.info.pos,
		profile = template.info.profile,
	},
	skills = template.skills,
	experiences = template.experiences,
	educations = base.education,
	languages = base.languages,
	interests = base.interests,
	certificates = base.certifications,
}
-- }}}

-- userinfo {{{
local function userinfo(f)

	local theinfo = info

	-- links {{{
	local links = {
		{
			name = "GitHub",
			url = true,
			desc = string.format('https://github.com/%s %s',
				theinfo.user.github, theinfo.user.github),
		},

		{
			name = "GitLab",
			url = true,
			desc = string.format('https://gitlab.com/%s %s',
				theinfo.user.github, theinfo.user.gitlab),
		},

		{
			name = "LinkedIn",
			url = true,
			desc = string.format("https://linkedin.com/in/%s %s",
				theinfo.user.linkedin, theinfo.user.linkedin),
		},

		{
			name = "Blog",
			url = true,
			desc = string.format("https://%s %s",
				theinfo.user.blog, theinfo.user.blog)
		},

		{
			name = "Location",
			url = false,
			desc = theinfo.user.location,
		},


		{
			name = "Phone",
			url = false,
			desc = string.format("%s", theinfo.user.phone),
		},

		{
			name = "Email",
			url = true,
			desc = string.format('mailto:%s %s', theinfo.user.email, theinfo.user.email),
		},

	}
	-- }}}

	io.write(".section Personal Information\n")
	io.write("\n")
	io.write(".SM\n")
	io.write(".SM\n")
	io.write(".fi\n")

	for key, value in pairs(links) do
		io.write(string.format("\\fB%s:\\fR\n", value.name))

		if value.url then
			io.write(string.format(".pdfhref W -D %s\n", value.desc))
		else
			io.write(string.format("%s\n", value.desc))
		end

		if key ~= #links then
			io.write(".br\n")
		end

	end

	io.write(string.format(".\n"))
end
-- }}}

-- skills {{{
local function skills(f)
	io.write(".section Skills\n")
	io.write(string.format(".nf\n"))
	-- io.write(string.format("\n"))

	for _, value in pairs(info.skills) do
		io.write(string.format(".subsection %s\n", value.name))
		-- io.write(string.format(".nf\n"))
		local sep = ","
		for k, v in pairs(value.details) do
			if k == #value.details then
				sep = ''
			end
			io.write(string.format(".skill \"%s%s\"\n", v, sep))
		end
	end
end
-- }}}

-- languages {{{
local function languages(f)
	io.write(".section Languages\n")
	io.write(string.format(".nf\n"))
	-- io.write(string.format("\n"))

	for _, value in pairs(info.skills) do
		io.write(string.format(".subsection %s\n", value.name))
		io.write(string.format(".nf\n"))
		for _, v in pairs(value.details) do
			io.write(string.format(".skill \"%s\"\n", v))
		end
	end
end
-- }}}

-- experiences {{{
local function experiences(f)
	io.write("\n")
	io.write(".section Work Experiences\n")
	io.write(".\n")
	io.write(".nf\n")

	for key, value in pairs(template.experiences) do
		io.write(string.format(".item \"%s\" \"%s\" \"%s\" \"%s\"\n",
			value.pos, value.name, value.date, value.loc))

		if value.details then
			io.write(string.format("%s\n", value.details))
		end

		if value.achieve then
			for _, v in pairs(value.achieve) do
				io.write(string.format(".list %s\n", v))
			end
		end
	end

end
-- }}}


-- init {{{

-- init left {{{
local function init_left(f)
	io.write("\n")
	io.write(".\n")
	io.write(".rt\n")
	io.write(".po 0.5i\n")
	io.write(".nr LL -2.4i\n")
	io.write(".nr FM 0.5i\n")

	skills(f)
	experiences(f)
end
-- }}}

-- init right {{{
local function init_right(f)
	io.write(".so resume.tmac\n")
	io.write(".\n")
	io.write(string.format(".title %s\n", "First and Last name"))
	io.write(string.format(".subtitle %s\n", "Your desiered position"))
	io.write(".\n")
	io.write(".sp -0.2i\n")
	io.write(".mk\n")
	io.write(".po 5.6i\n")

	userinfo(f)
end
-- }}}

-- }}}




local function resume()
	local file = io.open("t.ms", "w")

	init_right(file)

	init_left(file)

	file:close()
end

resume()


